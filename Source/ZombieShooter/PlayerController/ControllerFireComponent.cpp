﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ControllerFireComponent.h"

#include "EnhancedInputComponent.h"
#include "ZombieShooter/Character/WeaponComponent.h"


// Sets default values for this component's properties
UControllerFireComponent::UControllerFireComponent()
{
	
}

void UControllerFireComponent::IOnPawnPossessed_Implementation(APawn* OldPawn, APawn* NewPawn)
{
	PossessedPawn = NewPawn;
	if (!IsValid(PossessedPawn)) return;
	WeaponComponent = PossessedPawn->FindComponentByClass<UWeaponComponent>();
}

void UControllerFireComponent::ISetupInput_Implementation(UEnhancedInputComponent* EnhancedInputComponent)
{
	if (!IsValid(EnhancedInputComponent)) return;
	if (IsValid(FireInputAction))
	{
		EnhancedInputComponent->BindAction(FireInputAction, ETriggerEvent::Triggered, this, &UControllerFireComponent::StartFireAction);
	}
	if (IsValid(ReleaseInputAction))
	{
		EnhancedInputComponent->BindAction(ReleaseInputAction, ETriggerEvent::Triggered, this, &UControllerFireComponent::FinishFireAction);
	}
	if (IsValid(ReloadInputAction))
	{
		EnhancedInputComponent->BindAction(ReloadInputAction, ETriggerEvent::Triggered, this, &UControllerFireComponent::ReloadAction);
	}
}


// Called when the game starts
void UControllerFireComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UControllerFireComponent::StartFireAction(const FInputActionValue& InputActionValue)
{
	if (!IsValid(WeaponComponent)) return;
	WeaponComponent->StartFire();
}

void UControllerFireComponent::FinishFireAction(const FInputActionValue& InputActionValue)
{
	if (!IsValid(WeaponComponent)) return;
	WeaponComponent->StopFire();
}

void UControllerFireComponent::ReloadAction(const FInputActionValue& InputActionValue)
{
	if (!IsValid(WeaponComponent)) return;
	WeaponComponent->ReloadWeapon();
}


