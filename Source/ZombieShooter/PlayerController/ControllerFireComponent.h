﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ControllerComponentInterface.h"
#include "InputBindedInterface.h"
#include "Components/ActorComponent.h"
#include "ControllerFireComponent.generated.h"


class UWeaponComponent;
class UInputAction;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIESHOOTER_API UControllerFireComponent : public UActorComponent, public IInputBindedInterface, public IControllerComponentInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UControllerFireComponent();

	virtual void IOnPawnPossessed_Implementation(APawn* OldPawn, APawn* NewPawn) override;
	virtual void ISetupInput_Implementation(UEnhancedInputComponent* EnhancedInputComponent) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void StartFireAction(const FInputActionValue& InputActionValue);

	UFUNCTION()
	void FinishFireAction(const FInputActionValue& InputActionValue);

	UFUNCTION()
	void ReloadAction(const FInputActionValue& InputActionValue);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<UInputAction> FireInputAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<UInputAction> ReleaseInputAction;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<UInputAction> ReloadInputAction;

	UPROPERTY()
	TObjectPtr<APawn> PossessedPawn;

	UPROPERTY()
	TObjectPtr<UWeaponComponent> WeaponComponent;
};
