﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "HumanPlayerController.h"

#include "ControllerComponentInterface.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputBindedInterface.h"


// Sets default values
AHumanPlayerController::AHumanPlayerController()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AHumanPlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);
	auto ControllerComponents = GetComponentsByInterface(UControllerComponentInterface::StaticClass());
	for (auto* Component : ControllerComponents)
	{
		IControllerComponentInterface::Execute_IOnPawnPossessed(Component, nullptr, aPawn);
	}
}

// Called when the game starts or when spawned
void AHumanPlayerController::BeginPlay()
{
	Super::BeginPlay();
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()); IsValid(Subsystem))
	{
		Subsystem->AddMappingContext(GameplayMappingContext, 0);
	}
}

void AHumanPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent);
	auto InputBindedComponents = GetComponentsByInterface(UInputBindedInterface::StaticClass());
	for (auto& Component : InputBindedComponents)
	{
		IInputBindedInterface::Execute_ISetupInput(Component, EnhancedInputComponent);
	}
}
