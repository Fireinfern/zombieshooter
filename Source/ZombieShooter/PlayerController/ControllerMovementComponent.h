﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ControllerComponentInterface.h"
#include "InputActionValue.h"
#include "InputBindedInterface.h"
#include "Components/ActorComponent.h"
#include "ControllerMovementComponent.generated.h"


class UCharacterMovementComponent;
struct FInputActionValue;
class UInputAction;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIESHOOTER_API UControllerMovementComponent : public UActorComponent, public IInputBindedInterface, public IControllerComponentInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UControllerMovementComponent();

	virtual void ISetupInput_Implementation(UEnhancedInputComponent* EnhancedInputComponent) override;

	virtual void IOnPawnPossessed_Implementation(APawn* OldPawn, APawn* NewPawn) override;
	virtual void IOnPawnUnPossessed_Implementation() override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void MoveAction(const FInputActionValue& InputActionValue);

	UFUNCTION()
	void LookAction(const FInputActionValue& InputActionValue);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<UInputAction> MoveInputAction;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<UInputAction> LookInputAction;

	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<APawn> PossessedPawn;
	
};
