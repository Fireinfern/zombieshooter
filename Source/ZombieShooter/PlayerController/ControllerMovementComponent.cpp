﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ControllerMovementComponent.h"

#include "EnhancedInputComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"


// Sets default values for this component's properties
UControllerMovementComponent::UControllerMovementComponent()
{
	
}

void UControllerMovementComponent::ISetupInput_Implementation(UEnhancedInputComponent* EnhancedInputComponent)
{
	EnhancedInputComponent->BindAction(MoveInputAction, ETriggerEvent::Triggered, this, &ThisClass::MoveAction);
	EnhancedInputComponent->BindAction(LookInputAction, ETriggerEvent::Triggered, this, &ThisClass::LookAction);
}

void UControllerMovementComponent::IOnPawnPossessed_Implementation(APawn* OldPawn, APawn* NewPawn)
{
	PossessedPawn = NewPawn;
}

void UControllerMovementComponent::IOnPawnUnPossessed_Implementation()
{
	PossessedPawn = nullptr;
}


// Called when the game starts
void UControllerMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UControllerMovementComponent::MoveAction(const FInputActionValue& InputActionValue)
{
	if (!IsValid(PossessedPawn))
	{
		return;
	}
	const FVector2D MovementVector = InputActionValue.Get<FVector2D>();
	if (MovementVector.IsNearlyZero()) return;
	PossessedPawn->AddMovementInput(PossessedPawn->GetActorForwardVector(), MovementVector.Y);
	PossessedPawn->AddMovementInput(PossessedPawn->GetActorRightVector(), MovementVector.X);
}

void UControllerMovementComponent::LookAction(const FInputActionValue& InputActionValue)
{
	if (!IsValid(PossessedPawn))
	{
		return;
	}
	const FVector2D InputVector = InputActionValue.Get<FVector2D>();
	if (InputVector.IsNearlyZero()) return;
	APlayerController* PlayerController = GetOwner<APlayerController>();
	PlayerController->AddYawInput(InputVector.X);
	PlayerController->AddPitchInput(InputVector.Y);
}

