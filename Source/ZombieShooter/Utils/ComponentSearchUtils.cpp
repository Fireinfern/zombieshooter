﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ComponentSearchUtils.h"

UActorComponent* UComponentSearchUtils::GetValidatedComponentByClass(AActor* Target,
	TSubclassOf<UActorComponent> ComponentClass, EDataValidationResult& OutputExec)
{
	if (!IsValid(Target) || !IsValid(ComponentClass))
	{
		OutputExec = EDataValidationResult::Invalid;
		return nullptr;
	}
	UActorComponent* FoundComponent = Target->FindComponentByClass(ComponentClass);
	OutputExec = IsValid(FoundComponent) ? EDataValidationResult::Valid : EDataValidationResult::Invalid;
	return FoundComponent;
}

UActorComponent* UComponentSearchUtils::GetValidatedComponentByTag(AActor* Target,
	TSubclassOf<UActorComponent> ComponentClass, FName Tag, EDataValidationResult& OutputExec)
{
	if (!IsValid(Target) || !IsValid(ComponentClass) || !Tag.IsValid())
	{
		OutputExec = EDataValidationResult::Invalid;
		return nullptr;
	}
	auto ComponentsFound = Target->GetComponentsByTag(ComponentClass, Tag);
	if (ComponentsFound.IsEmpty())
	{
		OutputExec = EDataValidationResult::Invalid;
		return nullptr;
	}
	OutputExec = EDataValidationResult::Valid;
	return ComponentsFound[0];
}
