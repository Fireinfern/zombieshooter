﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ComponentSearchUtils.generated.h"

/**
 * 
 */
UCLASS()
class ZOMBIESHOOTER_API UComponentSearchUtils : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, meta=(DeterminesOutputType="ComponentClass", ExpandEnumAsExecs="OutputExec"))
	static UActorComponent* GetValidatedComponentByClass(AActor* Target, TSubclassOf<UActorComponent> ComponentClass,
	                                                     EDataValidationResult& OutputExec);

	UFUNCTION(BlueprintCallable, meta=(DeterminesOutputType="ComponentClass", ExpandEnumAsExecs="OutputExec"))
	static UActorComponent* GetValidatedComponentByTag(AActor* Target, TSubclassOf<UActorComponent> ComponentClass,
	                                                   FName Tag, EDataValidationResult& OutputExec);

	template<class T>
	static T* FindComponentByTag(AActor* Target, FName Tag);
};

template <class T>
T* UComponentSearchUtils::FindComponentByTag(AActor* Target, FName Tag)
{
	if (!IsValid(Target) || !Tag.IsValid())
	{
		return nullptr;
	}
	auto ComponentsWithTag = Target->GetComponentsByTag(T::StaticClass(), Tag);
	if (ComponentsWithTag.IsEmpty())
	{
		return nullptr;
	}
	return static_cast<T*>(ComponentsWithTag[0]);
}
