﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "DeathCameraComponent.h"

#include "HealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "ZombieShooter/Utils/ComponentSearchUtils.h"


// Sets default values for this component's properties
UDeathCameraComponent::UDeathCameraComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bAutoActivate = false;
}


// Called when the game starts
void UDeathCameraComponent::BeginPlay()
{
	Super::BeginPlay();
	HealthComponent = GetOwner()->FindComponentByClass<UHealthComponent>();
	if (ensure(IsValid(HealthComponent)))
	{
		HealthComponent->OnDied.AddDynamic(this, &ThisClass::StartDeathCameraTransition);
	}
}

void UDeathCameraComponent::StartDeathCameraTransition_Implementation()
{
	UCameraComponent* PrimaryCamera = UComponentSearchUtils::FindComponentByTag<UCameraComponent>(GetOwner(), FName("PrimaryCamera"));
	if (IsValid(PrimaryCamera))
	{
		PrimaryCamera->SetActive(false);
	}
	SetActive(true);
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	if (!IsValid(PlayerController)) return;
	PlayerController->SetViewTargetWithBlend(GetOwner(), 2.0f, VTBlend_EaseOut);
}

