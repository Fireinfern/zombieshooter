﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CharactersDeathComponent.h"

#include "HealthComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"


// Sets default values for this component's properties
UCharactersDeathComponent::UCharactersDeathComponent()
{
	
}


// Called when the game starts
void UCharactersDeathComponent::BeginPlay()
{
	Super::BeginPlay();
	HealthComponent = GetOwner()->FindComponentByClass<UHealthComponent>();
	if (ensure(HealthComponent))
	{
		HealthComponent->OnDied.AddDynamic(this, &UCharactersDeathComponent::Die);
	}
	SkeletalMeshComponent = GetOwner()->FindComponentByClass<USkeletalMeshComponent>();
}

void UCharactersDeathComponent::Die()
{
	UCapsuleComponent* CapsuleComponent = GetOwner()->FindComponentByClass<UCapsuleComponent>();
	if (IsValid(CapsuleComponent))
	{
		CapsuleComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	SkeletalMeshComponent->SetCollisionProfileName(TEXT("Ragdoll"));
	SkeletalMeshComponent->SetAllBodiesSimulatePhysics(true);
	SkeletalMeshComponent->SetSimulatePhysics(true);

	UCharacterMovementComponent* CharacterMovementComponent = GetOwner()->FindComponentByClass<UCharacterMovementComponent>();
	CharacterMovementComponent->Deactivate();
	CharacterMovementComponent->StopMovementImmediately();
	CharacterMovementComponent->DisableMovement();
}


