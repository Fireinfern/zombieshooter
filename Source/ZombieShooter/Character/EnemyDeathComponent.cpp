﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyDeathComponent.h"


// Sets default values for this component's properties
UEnemyDeathComponent::UEnemyDeathComponent()
{
	
}


void UEnemyDeathComponent::Die()
{
	Super::Die();
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.SetTimer(DisappearanceTimerHandle,this, &ThisClass::FinishDeath, 2.0f,  false);
}

void UEnemyDeathComponent::FinishDeath()
{
	GetOwner()->Destroy();
}
