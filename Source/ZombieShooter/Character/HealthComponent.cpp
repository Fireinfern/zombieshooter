﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"


// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OnTakenAnyDamage);
	CurrentHealth = MaxHealth;
}

void UHealthComponent::OnTakenAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.0f) return;
	const float OldHealth = CurrentHealth;
	CurrentHealth = FMath::Clamp(CurrentHealth - Damage, 0.0f, MaxHealth);
	OnDamageTaken.Broadcast(OldHealth, CurrentHealth, DamageCauser);
	if (CurrentHealth > 0.0f || OnDiedAlreadyCalled) return;
	OnDied.Broadcast();
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("%s Died"), *GetOwner()->GetName()));
	OnDiedAlreadyCalled = true;
}
