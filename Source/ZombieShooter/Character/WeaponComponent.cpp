﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponComponent.h"

#include "Camera/CameraShakeSourceComponent.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "ZombieShooter/Weapon/WeaponDataAsset.h"


// Sets default values for this component's properties
UWeaponComponent::UWeaponComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	FireBlockingTags.AddTag(Weapon_State_Blocked);
	FireBlockingTags.AddTag(Weapon_State_Reloading);
}

void UWeaponComponent::StartFire()
{
	if (!IsValid(CurrentWeaponDataAsset) || WeaponStateTag.MatchesAny(FireBlockingTags)) return;
	WeaponStateTag = Weapon_State_Fireing;
	Shoot();
}

void UWeaponComponent::StopFire()
{
	WeaponStateTag = Weapon_State_None;
}

void UWeaponComponent::ReloadWeapon()
{
	StopFire();
	if (!IsValid(CurrentWeaponDataAsset) || CurrentWeaponDataAsset->MagazineCapacity == 0) return;
	CurrentWeaponAmmo = CurrentWeaponDataAsset->MagazineCapacity;
}

void UWeaponComponent::EquipWeapon(UWeaponDataAsset* WeaponDataAsset)
{
	if (!IsValid(WeaponDataAsset)) return;
	CurrentWeaponDataAsset = WeaponDataAsset;
	UStaticMesh* Mesh = CurrentWeaponDataAsset->WeaponMesh.LoadSynchronous();
	if (IsValid(WeaponMeshComponent))
	{
		WeaponMeshComponent->SetStaticMesh(Mesh);
	}
}

bool UWeaponComponent::IsHoldingWeapon() const
{
	return IsValid(CurrentWeaponDataAsset);
}

void UWeaponComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                     FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (!WeaponStateTag.MatchesTag(Weapon_State_Fireing) || !IsHoldingWeapon())
	{
		return;
	}
	CurrentTimeLeftForShooting -= DeltaTime;
	if (CurrentTimeLeftForShooting <= 0.0f)
	{
		Shoot();
	}
}


// Called when the game starts
void UWeaponComponent::BeginPlay()
{
	Super::BeginPlay();
	auto PossibleMeshComponents = GetOwner()->GetComponentsByTag(UStaticMeshComponent::StaticClass(),
	                                                             FName("WeaponMesh"));
	if (!PossibleMeshComponents.IsEmpty())
	{
		WeaponMeshComponent = CastChecked<UStaticMeshComponent>(PossibleMeshComponents.Top());
		if (!IsValid(InitialWeaponDataAsset)) return;
		EquipWeapon(InitialWeaponDataAsset);
	}
	CameraShakeSourceComponent = GetOwner()->FindComponentByClass<UCameraShakeSourceComponent>();
	GunAudioComponent = GetOwner()->FindComponentByClass<UAudioComponent>();
}

void UWeaponComponent::Shoot()
{
	if (!IsValid(CurrentWeaponDataAsset)) return;
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	if (!IsValid(PlayerController)) return;
	FVector CameraLocation = PlayerController->PlayerCameraManager->GetCameraLocation();
	const FRotator CameraRotation = PlayerController->PlayerCameraManager->GetCameraRotation();
	CameraLocation += CameraRotation.Vector() * 10.0f;
	FHitResult Hit;
#if WITH_EDITOR
	DrawDebugLine(GetWorld(), CameraLocation, CameraLocation + (CameraRotation.Vector() * CurrentWeaponDataAsset->
		              MaxDistance), FColor::Red, false, 10.0f);
#endif
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(PlayerController);
	QueryParams.bTraceComplex = true;

	if (GetWorld()->LineTraceSingleByChannel(Hit, CameraLocation,
	                                         CameraLocation + (CameraRotation.Vector() * CurrentWeaponDataAsset->
		                                         MaxDistance), CollisionChannelType))
	{
		UPhysicalMaterial* HitPhysMat = Hit.PhysMaterial.Get();
		EPhysicalSurface SurfaceType = UPhysicalMaterial::DetermineSurfaceType(HitPhysMat);
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("%s SurfaceType"), SurfaceType));
		switch (SurfaceType)
		{
			case SurfaceType1:
				GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("HEAD SHOT SurfaceType1")));
				UGameplayStatics::ApplyDamage(Hit.GetActor(), CurrentWeaponDataAsset->WeaponBaseDamage * 10, PlayerController,
					GetOwner(), CurrentWeaponDataAsset->DamageType);
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BloodParticleSystem, Hit.Location);
				break;
			case SurfaceType2:
				GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("BODY SHOT SurfaceType1")));
				UGameplayStatics::ApplyDamage(Hit.GetActor(), CurrentWeaponDataAsset->WeaponBaseDamage, PlayerController,
					GetOwner(), CurrentWeaponDataAsset->DamageType);
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BloodParticleSystem, Hit.Location);
				break;
			default:
				GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("DEFAULT")));
				UGameplayStatics::ApplyDamage(Hit.GetActor(), CurrentWeaponDataAsset->WeaponBaseDamage, PlayerController,
					GetOwner(), CurrentWeaponDataAsset->DamageType);
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BloodParticleSystem, Hit.Location);
				break;
		}
	}
	CurrentTimeLeftForShooting = CurrentWeaponDataAsset->ShootCooldown;
	if (IsValid(GunAudioComponent))
	{
		GunAudioComponent->Play();
	}
	if (IsValid(CameraShakeSourceComponent))
	{
		CameraShakeSourceComponent->Start();
	}
}
