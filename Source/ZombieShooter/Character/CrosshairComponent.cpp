﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CrosshairComponent.h"

#include "HealthComponent.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"


// Sets default values for this component's properties
UCrosshairComponent::UCrosshairComponent()
{
	
}


// Called when the game starts
void UCrosshairComponent::BeginPlay()
{
	Super::BeginPlay();
	UHealthComponent* HealthComponent = GetOwner()->FindComponentByClass<UHealthComponent>();
	if (IsValid(HealthComponent))
	{
		HealthComponent->OnDied.AddDynamic(this, &ThisClass::RemoveCrosshair);
	}
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	if (!ensure(IsValid(PlayerController))) return;
	CrosshairWidget = CreateWidget<UUserWidget>(PlayerController, CrosshairWidgetClass);
	if (IsValid(CrosshairWidget))
	{
		CrosshairWidget->AddToViewport(0);
	}
}

void UCrosshairComponent::RemoveCrosshair()
{
	if (IsValid(CrosshairWidget))
	{
		CrosshairWidget->RemoveFromParent();
	}
}
