﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionComponent.h"


// Sets default values for this component's properties
UInteractionComponent::UInteractionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInteractionComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	auto PossibleInteractions = GetOwner()->GetComponentsByTag(UPrimitiveComponent::StaticClass(), InteractionTriggerTag);
	if (ensure(!PossibleInteractions.IsEmpty())) return;
	UPrimitiveComponent* TriggerComponent = Cast<UPrimitiveComponent>(PossibleInteractions.Top());
	TriggerComponent->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnInteractionEntered);
}

void UInteractionComponent::OnInteractionEntered(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}
