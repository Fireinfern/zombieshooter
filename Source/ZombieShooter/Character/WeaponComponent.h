﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ZombieShooter/Weapon/WeaponStateTags.h"
#include "WeaponComponent.generated.h"


struct FGameplayTag;
class UWeaponDataAsset;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable, BlueprintType)
class ZOMBIESHOOTER_API UWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UWeaponComponent();

	UFUNCTION(BlueprintCallable)
	void StartFire();

	UFUNCTION(BlueprintCallable)
	void StopFire();

	UFUNCTION(BlueprintCallable)
	void ReloadWeapon();

	UFUNCTION(BlueprintCallable)
	void EquipWeapon(UWeaponDataAsset* WeaponDataAsset);

	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool IsHoldingWeapon() const;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
		FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void Shoot();

	UPROPERTY(BlueprintReadWrite)
	FGameplayTag WeaponStateTag = Weapon_State_None;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FGameplayTagContainer FireBlockingTags;
	
	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<UWeaponDataAsset> CurrentWeaponDataAsset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TObjectPtr<UWeaponDataAsset> InitialWeaponDataAsset;

	UPROPERTY(BlueprintReadWrite)
	int32 CurrentWeaponAmmo = 0;

	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<UStaticMeshComponent> WeaponMeshComponent;

	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<UAudioComponent> GunAudioComponent;

	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<UCameraShakeSourceComponent> CameraShakeSourceComponent;
	
	UPROPERTY()
	float CurrentTimeLeftForShooting = 0.9f;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TEnumAsByte<ECollisionChannel> CollisionChannelType = ECC_Camera;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TObjectPtr<UParticleSystem> BloodParticleSystem;
};
