﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CharactersDeathComponent.h"
#include "EnemyDeathComponent.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIESHOOTER_API UEnemyDeathComponent : public UCharactersDeathComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UEnemyDeathComponent();

protected:
	virtual void Die() override;

	UFUNCTION()
	void FinishDeath();

	UPROPERTY()
	FTimerHandle DisappearanceTimerHandle;
};
