﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnDamageTaken, float, OldHealth, float, NewHealth, AActor*, Instigator);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDied);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ZOMBIESHOOTER_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(BlueprintAssignable)
	FOnDamageTaken OnDamageTaken;

	UPROPERTY(BlueprintAssignable)
	FOnDied OnDied;

	UFUNCTION(BlueprintCallable, BlueprintGetter)
	FORCEINLINE float GetCurrentHealth() const
	{
		return CurrentHealth;
	}

	UFUNCTION(BlueprintCallable, BlueprintGetter)
	FORCEINLINE float GetMaxHealth() const
	{
		return MaxHealth;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE bool IsDead() const
	{
		return CurrentHealth <= 0.0f;
	}
	
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnTakenAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(BlueprintReadWrite, BlueprintGetter=GetCurrentHealth)
	float CurrentHealth = 100.f;

	UPROPERTY(BlueprintReadOnly, BlueprintGetter=GetMaxHealth	, EditDefaultsOnly, meta=(ClampMin=1.f))
	float MaxHealth = 100.f;

	bool OnDiedAlreadyCalled;
};
