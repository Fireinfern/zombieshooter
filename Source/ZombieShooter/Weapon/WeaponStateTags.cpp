﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponStateTags.h"


UE_DEFINE_GAMEPLAY_TAG(Weapon_State_None, "Weapon.State.None");
UE_DEFINE_GAMEPLAY_TAG(Weapon_State_Fireing, "Weapon.State.Fireing");
UE_DEFINE_GAMEPLAY_TAG(Weapon_State_Reloading, "Weapon.State.Reloading");
UE_DEFINE_GAMEPLAY_TAG(Weapon_State_Blocked, "Weapon.State.Blocked");
