﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NativeGameplayTags.h"

UE_DECLARE_GAMEPLAY_TAG_EXTERN(Weapon_State_None);
UE_DECLARE_GAMEPLAY_TAG_EXTERN(Weapon_State_Fireing);
UE_DECLARE_GAMEPLAY_TAG_EXTERN(Weapon_State_Reloading);
UE_DECLARE_GAMEPLAY_TAG_EXTERN(Weapon_State_Blocked);
