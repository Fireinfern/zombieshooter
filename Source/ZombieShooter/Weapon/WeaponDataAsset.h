﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WeaponDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class ZOMBIESHOOTER_API UWeaponDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSoftObjectPtr<UStaticMesh> WeaponMesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float WeaponBaseDamage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 MagazineCapacity = 10;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float ShootCooldown = 0.9f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxDistance = 500.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<UDamageType> DamageType;
};
